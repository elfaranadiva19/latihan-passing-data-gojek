package com.example.lathan_data_intent_gojek_elfara;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class OrderProses extends AppCompatActivity implements View.OnClickListener{

    Button back;
    TextView datanama;
    TextView datalamat;
    TextView datapesanan;
    String nama,alamat,pesanan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_proses);
        back = findViewById(R.id.cancel);
        datanama = (TextView)findViewById(R.id.d_nama);
        datalamat = (TextView)findViewById(R.id.d_alamat);
        datapesanan = (TextView)findViewById(R.id.d_pesanan);
        back.setOnClickListener(this);
        getData();

    }
    public void getData() {
        nama = getIntent().getStringExtra("Nama");
        alamat = getIntent().getStringExtra("Alamat");
        pesanan = getIntent().getStringExtra("Pesanan");

        datanama.setText(nama);
        datalamat.setText(alamat);
        datapesanan.setText(pesanan);
    }

    @Override
    public void onClick(View view){
        Intent back = new Intent(OrderProses.this, MainActivity.class);
        startActivity(back);
    }
}
