package com.example.lathan_data_intent_gojek_elfara;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class OrderFood extends AppCompatActivity implements View.OnClickListener {

    Button pesanan;
    EditText edit_nama;
    EditText edit_alamat;
    EditText edit_pesanan;

    public static String EXTRA_SELECTED_VALUE = "extra_selected_value";
    public static int RESULT_CODE = 110;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_food);
        pesanan = findViewById(R.id.btn_pesan);
        pesanan.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        Intent pesanan = new Intent (OrderFood.this, OrderProses.class);
        edit_nama = findViewById(R.id.edt_textname);
        pesanan.putExtra("Nama",edit_nama.getText().toString());

        edit_alamat = findViewById(R.id.edt_textalamat);
        pesanan.putExtra("Alamat", edit_alamat.getText().toString());

        edit_pesanan = findViewById(R.id.edt_textpesanan);
        pesanan.putExtra("Pesanan",edit_pesanan.getText().toString());

        startActivity(pesanan);

    }
}
